/* Generated automatically. */
static const char configuration_arguments[] = "./configure --prefix=/home/simao/zodaai/toolchains/prebuilt/linux-x64/gcc/x64-linux-gnuabi";
static const char thread_model[] = "posix";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "cpu", "generic" }, { "arch", "x86-64" } };
